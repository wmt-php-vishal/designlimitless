<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::view('dashboard', 'dashboard')->name('dashboard');


//Route::get('/design', 'HomeController@design')->name('design');


Route::view('signUp', 'signUp')->name('signUp');

Route::view('signIn', 'signIn')->name('signIn');

Route::view('QAScreen', 'QAScreen')->name('QAScreen');

Route::view('multipleSelectQA', 'multipleSelectQA')->name('multipleSelectQA');

Route::view('findYourProperty', 'findYourProperty')->name('findYourProperty');

Route::view('FAQ', 'FAQ')->name('FAQ');

Route::view('partnerPage', 'partnerPage')->name('partnerPage');

Route::view('uploadEvidence', 'uploadEvidence')->name('uploadEvidence');

Route::view('QATextField', 'QATextField')->name('QATextField');









