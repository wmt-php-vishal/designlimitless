@extends('html')

@section('js')

    <!-- Theme JS files -->
    <script src="{{asset('assets/js/plugins/uploaders/purify.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/uploaders/sortable.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/uploaders/fileinput.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/uploader_bootstrap.js')}}"></script>
    <!-- /theme JS files -->

    @endsection


@section('title', 'Upload Evidence')


@section('content')

    <form action="#" enctype="multipart/form-data">
        <div class="card mt-5 " style="border-top: #21A7F6 5px solid">
            <div class="card-title">

                <h1 class="text-center mt-4 display-4 font-weight-normal">Upload Evidence</h1>
                <div class="heading-elements">
                </div>
            </div>


            <div class="card-body ">

                <div class="form-group row">

                    <div class="col-lg-12">
                        <input type="file" class="file-input-ajax" multiple="multiple" data-fouc>
                    </div>
                </div>
            </div>
        </div>
    </form>

    @endsection
