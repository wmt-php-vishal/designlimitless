@extends('html')

@section('js')

    @endsection


@section('title' , 'Partner Page')


@section('content')

    <form action="#" >
        <div class="card mt-5 p-1" style="border-top: #21A7F6 5px solid">
            <div class="card-header">

                <h3 class=" mt-4 font-weight-semibold">We're not going to be able to protest for you, but we have several good options available...</h3>

                <div class="heading-elements">
                    {{--                    <ul class="icons-list">--}}
                    {{--                        <li><a data-action="collapse"></a></li>--}}
                    {{--                        <li><a data-action="reload"></a></li>--}}
                    {{--                        <li><a data-action="close"></a></li>--}}
                    {{--                    </ul>--}}
                </div>
            </div>


            <div class="card-body ">
                <span>Why? The volume of people requestin our help has grown exponentially.Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
                    id est laborum.</span>

                <h3 class="mt-4 font-weight-semibold">We didn't  want to leave  you without at least some kind of help.</h3>
                <h4 class="font-weight-semibold">Here are couple questions:</h4>
                <h6 class="font-weight-bold mb-0">1) Hire a company for a fee:</h6>
                <p class="h5">If I was going to hire a company (other than our own). I would hire these folks. We share the similar values.
                    I secured a discount code for you. <a href="">www.propertytaxlock.com</a> Discount code <strong>Chandler2020</strong> </p>
                <h6 class="font-weight-bold mt-2 mb-0">2) Do it yourself:</h6>
                <p class="h5">We made this video because we wanted to equip you with some of the successful strategies we have used to negotiate staggering reductions.</p>
            </div>

        </div>
    </form>

    @endsection
