@extends('html')

@section('js')

    <style>
        .custom {
            width: 185px !important;
        }
    </style>

@endsection

@section('title' , 'Q A')

@section('content')

    <form action="#">
        <div class="card mt-5 " style="border-top: #21A7F6 5px solid">
            <div class="card-title">

                {{--                <h1 class="text-center mt-4 font-weight-bold">Sign In</h1>--}}

                <div class="heading-elements">
                    {{--                    <ul class="icons-list">--}}
                    {{--                        <li><a data-action="collapse"></a></li>--}}
                    {{--                        <li><a data-action="reload"></a></li>--}}
                    {{--                        <li><a data-action="close"></a></li>--}}
                    {{--                    </ul>--}}
                </div>
            </div>


            <div class="card-body ">
                <div class=" row ">
                    <div class="form-group col-lg-12 px-2">
                        <h1 class="font-weight-normal display-4 text-center">Where is your property located ?</h1>
                    </div>
                </div>

                <div class="row text-center  mb-5">
                    <div class="col-lg-12  ">
                        <button type="button" class="btn btn-primary btn-lg custom mx-2 my-2">Inside Tarrant Country</button>
                        <button type="button" class="btn btn-primary btn-lg custom my-2">Outside Tarrant Country</button>
                        {{--                        <a href="" class="btn btn-primary btn-lg  m-3">Inside Tarrant Country</a>--}}
                        {{--                        <a href="" class="btn btn-primary btn-lg ">Outside Tarrant Country</a>--}}
                    </div>
                </div>


            </div>

        </div>
    </form>


@endsection
