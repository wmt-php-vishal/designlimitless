@extends('html')

@section('js')

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/ui/prism.min.js')}}"></script>


    <script type="text/javascript" src="{{asset('assets/js/pages/datatables_basic.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <link href="{{asset('css/dashboard.css')}}" rel="stylesheet" type="text/css">
    {{--    <script type="text/javascript" src="{{asset('assets/js/plugins/ui/ripple.min.js')}}"></script>--}}
    <!-- Theme JS files end -->

    <style>
        table {
            border-left: 1px solid rgb(221, 221, 221);
            border-right: 1px solid rgb(221, 221, 221);
            border-bottom: 1px solid rgb(221, 221, 221);
            /*border: 1px solid rgb(221,221,221);*/

            word-wrap: break-word;
        }

    </style>
@endsection

@section('title' , 'Design page')

@section('content')

    <div class="row">
        <div class="col-lg-6 col-sm-12 col-12"><h3 class="font-weight-semibold">Dashboard</h3></div>
        <div class="col-lg-6 col-sm-12 col-12"><a href="" class="btn btn-primary   float-lg-right ">Apply New Post</a></div>
    </div>

    <!-- Highlighted tabs -->
    <div class="row mt-4">


        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h6 class="card-title font-weight-semibold">Protest Details</h6>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-highlight  nav-tabs-bottom nav-">
                        <li class="nav-item"><a href="#highlighted-justified-tab1" class="nav-link active"
                                                data-toggle="tab">Applied Protest</a></li>
                        <li class="nav-item"><a href="#highlighted-justified-tab2" class="nav-link" data-toggle="tab">Drafts</a>
                        </li>

                    </ul>

                    <div class="tab-content mx-4">
                        <div class="tab-pane fade show active" id="highlighted-justified-tab1">
                            <table class="table datatable-basic">
                                <thead>
                                <tr>
                                    <th>Account Number</th>
                                    <th>Owner Name</th>
                                    <th>Address</th>
                                    <th>Date & Time</th>
                                    <th>Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>96321546</td>
                                    <td>Adam Parkins</td>
                                    <td>70 South Phil Ave, Round Rock, TX 78681 </td>
                                    <td>3 Oct 1981</td>
                                    <td><span class="badge badge-primary">Active</span></td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i>
                                                        Export to .pdf</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-excel"></i>
                                                        Export to .csv</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-word"></i>
                                                        Export to .doc</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>

                        <div class="tab-pane fade" id="highlighted-justified-tab2">

                            <table class="table datatable-basic">
                                <thead>
                                <tr>
                                    <th>Account Number</th>
                                    <th>Owner Name</th>
                                    <th>Address</th>
                                    <th>Date & Time</th>
                                    <th>Status</th>
{{--                                    <th class="text-center">Actions</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>96321546</td>
                                    <td>Adam Parkins</td>
                                    <td>70 South Phil Ave, Round Rock, TX 78681 </td>
                                    <td>3 Oct 1981</td>
                                    <td><span class="badge badge-primary">Active</span></td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i>
                                                        Export to .pdf</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-excel"></i>
                                                        Export to .csv</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-word"></i>
                                                        Export to .doc</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>96321546</td>
                                    <td>Adam Parkins</td>
                                    <td>70 South Phil Ave, Round Rock, TX 78681 </td>
                                    <td>3 Oct 1981</td>
                                    <td><span class="badge badge-primary">Active</span></td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i>
                                                        Export to .pdf</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-excel"></i>
                                                        Export to .csv</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-word"></i>
                                                        Export to .doc</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>96321546</td>
                                    <td>Adam Parkins</td>
                                    <td>70 South Phil Ave, Round Rock, TX 78681 </td>
                                    <td>3 Oct 1981</td>
                                    <td><span class="badge badge-primary">Active</span></td>
                                    <td class="text-center">
                                        <div class="list-icons">
                                            <div class="dropdown">
                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i>
                                                        Export to .pdf</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-excel"></i>
                                                        Export to .csv</a>
                                                    <a href="#" class="dropdown-item"><i class="icon-file-word"></i>
                                                        Export to .doc</a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>



                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /highlighted tabs -->



    <!-- Basic datatable -->
    {{--    <div class="card mt-2">--}}
    {{--        <div class="card-header header-elements-inline">--}}
    {{--            <h5 class="card-title">Applied Protests</h5>--}}
    {{--            <div class="header-elements">--}}
    {{--                <div class="list-icons">--}}
    {{--                    <a class="list-icons-item" data-action="collapse"></a>--}}
    {{--                    <a class="list-icons-item" data-action="reload"></a>--}}
    {{--                    <a class="list-icons-item" data-action="remove"></a>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}

    {{--        <div class="card-body">--}}


    {{--        <table class="table datatable-basic">--}}
    {{--            <thead>--}}
    {{--            <tr>--}}
    {{--                <th>First Name</th>--}}
    {{--                <th>Last Name</th>--}}
    {{--                <th>Job Title</th>--}}
    {{--                <th>DOB</th>--}}
    {{--                <th>Status</th>--}}
    {{--                <th class="text-center">Actions</th>--}}
    {{--            </tr>--}}
    {{--            </thead>--}}
    {{--            <tbody>--}}
    {{--            <tr>--}}
    {{--                <td>Marth</td>--}}
    {{--                <td><a href="#">Enright</a></td>--}}
    {{--                <td>Traffic Court Referee</td>--}}
    {{--                <td>22 Jun 1972</td>--}}
    {{--                <td><span class="badge badge-success">Active</span></td>--}}
    {{--                <td class="text-center">--}}
    {{--                    <div class="list-icons">--}}
    {{--                        <div class="dropdown">--}}
    {{--                            <a href="#" class="list-icons-item" data-toggle="dropdown">--}}
    {{--                                <i class="icon-menu9"></i>--}}
    {{--                            </a>--}}

    {{--                            <div class="dropdown-menu dropdown-menu-right">--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to .pdf</a>--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to .csv</a>--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-word"></i> Export to .doc</a>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </td>--}}
    {{--            </tr>--}}
    {{--            <tr>--}}
    {{--                <td>Marth</td>--}}
    {{--                <td><a href="#">Enright</a></td>--}}
    {{--                <td>Traffic Court Referee</td>--}}
    {{--                <td>22 Jun 1972</td>--}}
    {{--                <td><span class="badge badge-success">Active</span></td>--}}
    {{--                <td class="text-center">--}}
    {{--                    <div class="list-icons">--}}
    {{--                        <div class="dropdown">--}}
    {{--                            <a href="#" class="list-icons-item" data-toggle="dropdown">--}}
    {{--                                <i class="icon-menu9"></i>--}}
    {{--                            </a>--}}

    {{--                            <div class="dropdown-menu dropdown-menu-right">--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to .pdf</a>--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to .csv</a>--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-word"></i> Export to .doc</a>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </td>--}}
    {{--            </tr>--}}


    {{--            <tr>--}}
    {{--                <td>Jackelyn</td>--}}
    {{--                <td>Weible</td>--}}
    {{--                <td><a href="#">Airline Transport Pilot</a></td>--}}
    {{--                <td>3 Oct 1981</td>--}}
    {{--                <td><span class="badge badge-secondary">Inactive</span></td>--}}
    {{--                <td class="text-center">--}}
    {{--                    <div class="list-icons">--}}
    {{--                        <div class="dropdown">--}}
    {{--                            <a href="#" class="list-icons-item" data-toggle="dropdown">--}}
    {{--                                <i class="icon-menu9"></i>--}}
    {{--                            </a>--}}

    {{--                            <div class="dropdown-menu dropdown-menu-right">--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to .pdf</a>--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to .csv</a>--}}
    {{--                                <a href="#" class="dropdown-item"><i class="icon-file-word"></i> Export to .doc</a>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </td>--}}
    {{--            </tr>--}}
    {{--            </tbody>--}}
    {{--        </table>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    <!-- /basic datatable -->



@endsection


