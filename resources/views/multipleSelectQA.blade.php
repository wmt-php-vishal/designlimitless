@extends('html')

@section('js')

    <script src="{{asset('assets/css/multipleSelectQA.css')}}"></script>
    <style>
        .bgblue:hover{
            color: white;
            background-color: #21A7F6;
        }
        .custom{
            width: 116px !important;
        }



    </style>
    @endsection



@section('title' , 'Multiple Select Q/A')


@section('content')

    <form action="#">
        <div class="card mt-5 " style="border-top: #21A7F6 5px solid">
            <div class="card-title">
                <div class="heading-elements">
                    {{--                    <ul class="icons-list">--}}
                    {{--                        <li><a data-action="collapse"></a></li>--}}
                    {{--                        <li><a data-action="reload"></a></li>--}}
                    {{--                        <li><a data-action="close"></a></li>--}}
                    {{--                    </ul>--}}
                </div>
            </div>


            <div class="card-body ">
                <div class=" row ">
                    <div class="form-group col-lg-12  px-2">
                        <h1 class="font-weight-normal display-4 text-center">Please rate the condition of your house.</h1>
                    </div>
                </div>

                <div class="row d-flex justify-content-center">
{{--                    <div class="col-lg-12 d-flex justify-content-center">--}}
                        <div class=" col-"><a href="" class="btn btn-light bgblue custom mr-2 my-2">Excellent</a></div>
                        <div class=" col-"><a href="" class="btn btn-light bgblue custom mr-2 my-2">Very Good</a></div>
                        <div class=" col-"><a href="" class="btn btn-light bgblue custom mr-2 my-2">Below Average</a></div>
                        <div class=" col-"><a href="" class="btn btn-light bgblue custom mr-2 my-2">Poor</a></div>


{{--                    </div>--}}
                </div>
                <br><br>

            </div>
        </div>
        <div>
            <button class="btn btn-light bg-white float-right">Back</button>
        </div>
    </form >


    @endsection
