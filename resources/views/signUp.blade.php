@extends('html')

@section('js')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/pages/form_layouts.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/plugins/ui/ripple.min.js')}}"></script>


    <script src="assets/js/plugins/forms/inputs/inputmask.js"></script>


@endsection


@section('title' , 'Design 2')


@section('content')

    <!-- Basic layout-->
    <form action="#">
        <div class="card mt-5 " style="border-top: #21A7F6 5px solid" >
            <div class="card-title">
                <h1 class="text-center mt-4 font-weight-semibold">Sign Up</h1>
                <div class="heading-elements">
{{--                    <ul class="icons-list">--}}
{{--                        <li><a data-action="collapse"></a></li>--}}
{{--                        <li><a data-action="reload"></a></li>--}}
{{--                        <li><a data-action="close"></a></li>--}}
{{--                    </ul>--}}
                </div>
            </div>


            <div class="card-body ">
                <div class="row ">
                    <div class="form-group col-lg px-4">
                        <label class="label">First Name *</label>
                        <input type="text" autocomplete="off" class="form-control" placeholder="Enter First Name">
                    </div>

                    <div class="form-group col-lg px-4">
                        <label>Last Name *</label>
                        <input type="text" class="form-control" placeholder="Your Last Name">
                    </div>
                </div>

                <div class=" row">
                    <div class="form-group col-lg-6 px-4">
                        <label>Email Address *</label>
                        <input type="email" autocomplete="off" class="form-control" placeholder="Enter Email Address">
                    </div>

                    <div class="form-group col-lg-6 px-4">
                        <label>Password *</label>
                        <input type="password" class="form-control" data-toggle="password" placeholder="Your Password">

                    </div>
                </div>

                <div class=" row">
                    <div class="form-group col-lg-6 px-4">
                        <label>Phone Number *</label>
                        <input type="text" autocomplete="off" data-mask="999-999-9999" class="form-control" placeholder="XXX-XXX-XXXX">
                    </div>

                    <div class="form-group col-lg-6 px-4">
                        <label>Alternate Phone Number</label>
{{--                        <input type="text" class="form-control" data-mask="999/999/9999" placeholder="Enter starting date">--}}
                        <input type="text" class="form-control" data-mask="999-999-9999" placeholder="XXX-XXX-XXXX">
                    </div>
                </div>
                <div class="text-right ">
                    <button type="submit" class="btn btn-light  mr-2">Cancel</button>
                    <button type="submit" class="btn btn-primary">Sign Up </button>
                </div>

            </div>
        </div>


    </form>
    <!-- /basic layout -->



@endsection


