@extends('html')

@section('js')

    @endsection

@section('title', 'FAQs')


@section('content')



    <form action="#">
        <div class="card mt-5 " style="border-top: #21A7F6 5px solid" >
            <div class="card-title">
                <h1 class="text-center mt-4 display-4 font-weight-semibold">FAQs</h1>
                <div class="heading-elements">
                    {{--                    <ul class="icons-list">--}}
                    {{--                        <li><a data-action="collapse"></a></li>--}}
                    {{--                        <li><a data-action="reload"></a></li>--}}
                    {{--                        <li><a data-action="close"></a></li>--}}
                    {{--                    </ul>--}}
                </div>
            </div>

            <div class="card-group-control p-3 card-group-control-left" id="accordion-control">
                <div class="card">
                    <div class="card-header p-2">
                        <h6 class="card-title">
                            <a data-toggle="collapse" class="text-default" href="#accordion-control-group1">When will my protest be complete ?</a>
                        </h6>
                    </div>

                    <div id="accordion-control-group1" class="collapse " data-parent="#accordion-control">
                        <div class="card-body">
                            Filling the protest and completing the protest are two different events. Your protest will be filled
                            before the May 15 deadline. We will attempt to complete  your protest as quick as possible.
                            They don't give us a timeline on when the hearing will be scheduled.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header p-2">
                        <h6 class="card-title">
                            <a class="collapsed text-default" data-toggle="collapse" href="#accordion-control-group2">How will i know when my protest is complete ?</a>
                        </h6>
                    </div>

                    <div id="accordion-control-group2" class="collapse" data-parent="#accordion-control">
                        <div class="card-body">
                            Тon cupidatat skateboard dolor brunch. Тesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header p-2">
                        <h6 class="card-title">
                            <a class="collapsed text-default" data-toggle="collapse" href="#accordion-control-group3">How can I find out the status of my protest ?</a>
                        </h6>
                    </div>

                    <div id="accordion-control-group3" class="collapse" data-parent="#accordion-control">
                        <div class="card-body">
                            3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header p-2">
                        <h6 class="card-title">
                            <a class="collapsed text-default" data-toggle="collapse" href="#accordion-control-group4">Who will send me email ?</a>
                        </h6>
                    </div>

                    <div id="accordion-control-group4" class="collapse" data-parent="#accordion-control">
                        <div class="card-body">
                            Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header p-2">
                        <h6 class="card-title">
                            <a class="collapsed text-default" data-toggle="collapse" href="#accordion-control-group5">Is it necessary for me to send you photos or repair quotes ?</a>
                        </h6>
                    </div>

                    <div id="accordion-control-group5" class="collapse" data-parent="#accordion-control">
                        <div class="card-body">
                            3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it
                            Food truck quinoa nesciunt laborum eiusmod. Brunch .
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header p-2">
                        <h6 class="card-title">
                            <a class="collapsed text-default" data-toggle="collapse" href="#accordion-control-group6">What can i do to help you ?</a>
                        </h6>
                    </div>

                    <div id="accordion-control-group6" class="collapse" data-parent="#accordion-control">
                        <div class="card-body">
                            3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it.
                        </div>
                    </div>
                </div>

            </div>
        <!-- /accordion with left control button -->

        </div>


    </form>

@endsection
