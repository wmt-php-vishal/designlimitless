@extends('html')

@section('js')

    <script src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/inputs/touchspin.min.js')}}"></script>

    <script src="{{asset('assets/js/core/app.js')}}"></script>
    <script src="{{asset('assets/js/pages/form_input_groups.js')}}"></script>

    <script src="{{asset('assets/js/pages/form_inputs.js')}}"></script>


    <script src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
{{--    <script src="{{asset('assets/js/pages/datatables_basic.js')}}"></script>--}}

{{--    <script src="{{asset('assets/js/paginationFalse.min.js')}}"></script>--}}
{{--    <script src="{{asset('assets/js/pages/datatables_advanced.js')}}"></script>--}}
    <!-- /theme JS files -->

    <style>
        table{
            /*border-left: 1px solid rgb(221,221,221);*/
            /*border-right: 1px solid rgb(221,221,221);*/
            border: 1px solid rgb(221,221,221);
        }

        @media only screen and (max-width: 991px) {
            .searchBar {
                /*display: flex;*/
                /*justify-content: center;*/


            }
            .input-group-prepend{
                /*margin-bottom: 10px;*/
                display: flex;
                justify-content: center;
                /*justify-items: center;*/

            }
        }

        /*@media only screen and (max-width: 1200) and (min-width: 991px) {*/
        /*    .searchTextBar{*/
        /*        margin-left: 2px;*/
        /*    }*/
        /*}*/

    </style>

@endsection


@section('title' , 'Find Your Property')


@section('content')

    <form action="#" class="">
        <div class="card mt-5 " style="border-top: #21A7F6 5px solid">
            <div class="card-title">
                <p class="text-center display-4 mt-4 font-weight-normal">Find Your Property</p>
            </div>


            <div class="card-body ">
                <div class="searchBar  row">
                    <div class="col-xl-2 col-lg-2 col-md-3 col-sm-6 col-12">
                        <div class="input-group d-flex justify-content-center">
                            <div class="input-group-prepend mb-2 ">
                                <button type="button" class="btn btn-light  dropdown-toggle bg-white px-xl-4 px-lg-2 mr-0 px-md-2 px-5 rounded-right" data-toggle="dropdown">Property Address</button>
                                <div class=" row dropdown-menu">
                                    <a href="" class="dropdown-item">Property Address</a>
                                    <a href="" class="dropdown-item">Owner Name</a>
                                    <a href="" class="dropdown-item">Account Number</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-9 col-lg-9 col-md-7 pr-lg-1 col-sm-6 col-12 searchTextBar mb-2  pr-0">
                        <input type="text" class="form-control " placeholder="Search by Property Address, Owner Name, Account Number">
                    </div>
                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-12 text-center ">
                        <button class="btn btn-primary px- ">Search</button>
                    </div>

                </div>

                <div class=" mb-1">

                    <h4 class="font-weight-semibold mt-5">Please Select Your Property</h4>


                    <table id="dataTable" class="table dataTable datatable-basic">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Job Title</th>
                            <th>DOB</th>
                            <th>Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Marth</td>
                            <td><a href="#">Enright</a></td>
                            <td>Traffic Court Referee</td>
                            <td>22 Jun 1972</td>
                            <td><span class="badge badge-success">Active</span></td>
                            <td class="text-center">
                                <div class="list-icons">
                                    <div class="dropdown">
                                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to .pdf</a>
                                            <a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to .csv</a>
                                            <a href="#" class="dropdown-item"><i class="icon-file-word"></i> Export to .doc</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Jackelyn</td>
                            <td>Weible</td>
                            <td><a href="#">Airline Transport Pilot</a></td>
                            <td>3 Oct 1981</td>
                            <td><span class="badge badge-secondary">Inactive</span></td>
                            <td class="text-center">
                                <div class="list-icons">
                                    <div class="dropdown">
                                        <a href="#" class="list-icons-item" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to .pdf</a>
                                            <a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to .csv</a>
                                            <a href="#" class="dropdown-item"><i class="icon-file-word"></i> Export to .doc</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>



{{--                    <table class="table datatable-show-all" >--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>Owner Name</th>--}}
{{--                            <th>Property Address</th>--}}
{{--                            <th>City/State</th>--}}
{{--                            <th>Zip Code</th>--}}
{{--                            <th class="text-center">Is This Your Property</th>--}}
{{--                       </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        <tr>--}}
{{--                            <td>Marth</td>--}}
{{--                            <td>Enright</td>--}}
{{--                            <td>Traffic Court Referee</td>--}}
{{--                            <td>22 Jun 1972</td>--}}
{{--                            <td class="text-center"><button class="btn btn-primary px-3 ">Yes</button></td>--}}

{{--                        </tr>--}}

{{--                        </tbody>--}}
{{--                    </table>--}}


{{--                    <table class="table table-bordered table-hover datatable-highlight" >--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>First Name</th>--}}
{{--                            <th>Last Name</th>--}}
{{--                            <th>Job Title</th>--}}
{{--                            <th>DOB</th>--}}
{{--                            <th>Is this your property</th>--}}

{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        <tr>--}}
{{--                            <td>Marth</td>--}}
{{--                            <td><a href="#">Enright</a></td>--}}
{{--                            <td>Traffic Court Referee</td>--}}
{{--                            <td>22 Jun 1972</td>--}}
{{--                            <td><button class="btn btn-primary ">Yes</button></td>--}}
{{--                        </tr>--}}
{{--                        </tbody>--}}
{{--                    </table>--}}

                </div>

            </div>




        </div>

        <div class="row mb-3">
            <button class="col- btn btn-light bg-white btn-lg">No matching address</button>
            <button class="btn btn-light bg-white btn-lg">Owner name not matched</button>
            <button class="btn btn-light bg-white float-right float-sm-centre btn-lg">Back</button>

        </div>
    </form>


    <script>
        $(document).ready(function() {

            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                columnDefs: [{
                    orderable: false,
                    width: 100,
                    targets: [ 5 ]
                }],
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                }
            });


            $('#dataTable').DataTable( {
                "paging":   false,
                "ordering": false,
                "info":     false,
                "searching": false
            } );
        } );

    </script>


@endsection




