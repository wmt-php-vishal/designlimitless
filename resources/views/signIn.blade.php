@extends('html')

@section('js')



@endsection


@section('title' , 'Sign In')


@section('content')

    <form action="#">
        <div class="card mt-5 " style="border-top: #21A7F6 5px solid">
            <div class="card-title">

                <h1 class="text-center mt-4 font-weight-bold">Sign In</h1>

                <div class="heading-elements">
                    {{--                    <ul class="icons-list">--}}
                    {{--                        <li><a data-action="collapse"></a></li>--}}
                    {{--                        <li><a data-action="reload"></a></li>--}}
                    {{--                        <li><a data-action="close"></a></li>--}}
                    {{--                    </ul>--}}
                </div>
            </div>


            <div class="card-body ">
                <div class=" row">
                    <div class="form-group col-lg-6 px-2">
                        <label>Email Address *</label>
                        <input type="email" autocomplete="off" class="form-control" placeholder="Enter Email Address">
                    </div>

                    <div class="form-group col-lg-6 px-2">
                        <label>Password *</label>
                        <input type="password" class="form-control" data-toggle="password" placeholder="Enter Password">
                    </div>
                </div>

                <div class="row float-right">
                    <div class="col-lg-12 ">
                        <button class="btn btn-light  mx-2">Cancel</button>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                    </div>
                </div>
                <br><br>
                <div class="row alpaca-float-right my-3">
                    <div class="col-lg-12 ">
                        <a href="">Forgot Your Password ?</a>
                    </div>
                </div>
            </div>




        </div>
    </form  >

@endsection
