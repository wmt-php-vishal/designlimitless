{{--<!-- Footer -->--}}
{{--<div class="navbar navbar-default navbar-fixed-bottom ">--}}
{{--    <ul class="nav navbar-nav no-border visible-xs-block">--}}
{{--        <li><a class="text-center " data-toggle="" data-target="#navbar-second"><i class="icon-circle-up2"></i></a></li>--}}
{{--    </ul>--}}

{{--    <div class="navbar-collapse collapse" id="navbar-second">--}}
{{--        <div class="navbar-text">--}}
{{--            &copy; 2020. <a href="#">Limitless</a>--}}
{{--        </div>--}}

{{--        <div class="navbar-right">--}}
{{--            <ul class="nav navbar-nav">--}}
{{--                <li><a href="#">Help center</a></li>--}}
{{--                <li><a href="">Back to home</a></li>--}}

{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- /footer -->


<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light navbarStyle">



    <div class="navbar-collapse  " id="navbar-footer">

			<span class="navbar-text ml-lg-auto">
				Copyright &copy; 2020 by <a href="">Chandler Crouch Group</a>
			</span>

        <ul class="navbar-nav ml-lg-auto">
            <li class="nav-item"><a href="" class="navbar-nav-link text-primary" target="_blank"> Consumer Protection
                    Notice</a>
            </li>
            <li class="nav-item"><a href="" class="navbar-nav-link text-primary" target="_blank"> Information About
                    Brokerage
                    Services</a></li>
        </ul>

        <span class="navbar-text mx-auto ">
            Designed and Developed by <img src="{{asset('assets/images/webmob.png')}}" alt="">
        </span>

    </div>
</div>

<!-- /footer -->
<style>
    /*.page-footer{*/
    /*    position: absolute;*/
    /*    height: auto;*/
    /*    right: 0;bottom:0;left:0;*/
    /*}*/

    .navbarStyle {
        display: flex;
        flex-direction: column;
        margin-top: auto;
    }

</style>
