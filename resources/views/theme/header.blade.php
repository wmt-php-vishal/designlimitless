<!-- /default navbar -->

<div class="navbar navbar-lg navbar-component  navbar-expand-xl " style="background-color: #63748A">

    <div class="navbar-nav">
        <a href="index.html" class="d-inline-block">
            <img src="{{asset('assets/images/logo.png')}}" alt="">
        </a>
    </div>

    <div class="d-xl-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-demo1-mobile">
            <i class="icon-grid3"></i>
        </button>
    </div>

    <div class="navbar-collapse  collapse" id="navbar-demo1-mobile">

        {{--User logged in starts here--}}
        {{--        <ul class="navbar-nav ">--}}
        {{--            <li class="nav-item"><button class="btn text-white navbar-nav-link ">Dashboard</button></li>--}}
        {{--            <li class="nav-item"><button class="btn text-white navbar-nav-link ">My Account</button></li>--}}
        {{--        </ul>--}}

        {{--User logged in starts here--}}

        <ul class="navbar-nav ml-xl-auto ">
            {{--  User not logged in--}}

            <li class="nav-item">
                <button class="navbar-nav-link btn text-white" data-toggle="modal" data-target="#modal_form_vertical">Check Application Status</button>
            </li>
            <li class="nav-item">
                <a href="{{route('signIn')}}" class="navbar-nav-link btn text-white mr-1">Sign In</a>
            </li>
            <li class="nav-item">
                <a href="{{route('signUp')}}" class="navbar-nav- btn btn-primary btn-sm d-flex justify-content-center mt-2  text-white">Sign Up</a>
            </li>

            {{--  User not logged in ends here--}}

            {{--            <li class="nav-item dropdown">--}}
            {{--                <a href="#" class="navbar-nav-link dropdown-toggle text-white" data-toggle="dropdown">--}}
            {{--                    {{ 'Adam' }}--}}
            {{--                </a>--}}

            {{--                <div class="dropdown-menu dropdown-menu-right">--}}
            {{--                    <a href="#" class="dropdown-item">Log Out</a>--}}
            {{--                </div>--}}
            {{--            </li>--}}
        </ul>
    </div>

</div>

<!-- Vertical form modal -->
<div id="modal_form_vertical" class="modal mt-2 fade" tabindex="-1" >
    <div class="modal-dialog  modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title font-weight-semibold">Check Application Status</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body m-0 py-0">
                <span>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</span>
            </div>
            <form action="#">
                <div class="modal-body">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Enter Account Number</label>
                                <input type="text" placeholder="Enter Account Number" class="form-control">
                            </div>


                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light btn-sm " data-dismiss="modal">Cancel</button>

                    <button class="btn bg-primary btn-sm" id="submit_btn" data-toggle="modal" data-target="#application_status">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /vertical form modal -->


<!-- Vertical form modal -->
{{--<div id="application_status" class="modal mt-2 fade" tabindex="-1">--}}
{{--    <div class="modal-dialog  modal-dialog modal-dialog-centered" >--}}
{{--        <div class="modal-content" style="width: 899px; height: auto">--}}
{{--            <div class="modal-header">--}}
{{--                <h4 class="modal-title font-weight-semibold">Application Status</h4>--}}
{{--                <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--            </div>--}}
{{--            <form action="#">--}}
{{--                <div class="modal-body">--}}

{{--                    <div class="form-group">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-12">--}}
{{--                                <label>Your application is in list. Please login with your registered ID to check--}}
{{--                                in detail.</label>--}}
{{--                            </div>--}}


{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}

{{--                <div class="modal-footer">--}}
{{--                    <button type="button" class="btn btn-light btn-sm " data-dismiss="modal">Cancel</button>--}}

{{--                    <button type="submit" class="btn bg-primary btn-sm" data-toggle="modal" >Sign In</button>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<!-- /vertical form modal -->

{{----}}

{{--<script>--}}
{{--    $("#submit_btn").click(function(){--}}
{{--        $("#modal_form_vertical").hide();--}}
{{--    });--}}
{{--</script>--}}
