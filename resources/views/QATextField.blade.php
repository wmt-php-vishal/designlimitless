@extends('html')

@section('js')

    <style>
        .greyLine {
            border-top: #E7E7E7 2px solid

        }
    </style>
@endsection


@section('title' , 'QA Text Field')


@section('content')

    <form action="#">
        <div class="card mt-5 " style="border-top: #21A7F6 5px solid">
            <div class="card-title">

                <h3 class="text-center display-4 mt-4 font-weight-normal">Did you purchase this property ?</h3>

                <div class="heading-elements">
                    {{--                    <ul class="icons-list">--}}
                    {{--                        <li><a data-action="collapse"></a></li>--}}
                    {{--                        <li><a data-action="reload"></a></li>--}}
                    {{--                        <li><a data-action="close"></a></li>--}}
                    {{--                    </ul>--}}
                </div>
            </div>


            <div class="card-body ">
                <div class=" row">
                    <div class="col-12 text-center">
                        <button class="btn btn-primary px-4 mr-2 btn-lg" value="Yes">Yes</button>
                        <button class="btn btn-primary px-4 btn-lg" value="No" id="btn_no">No</button>
                    </div>
                </div>


                <form action="" class=" ">
                    <div class="greyLine my-4 text-center " id="yearInputBox">

                        <div class="row text-center mt-3">
                            <div class="col-12">
                                <h3>What year did you purchase the property ?</h3>
                            </div>
                        </div>


                        <div class="row inputboxBtn ">
                            <input type="text" class="form-control col-xl-3 col-lg-3 col-md-6 col-sm-6 col-7 offset-xl-4 offset-lg-4 offset-md-2 offset-sm-2 offset-1 mr-xl-2 mr-lg-2 mr-md-2 mr-2"
                                   placeholder="Enter Year">
                            <button type="submit" class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-3  btn btn-primary px-3">Next</button>
                        </div>

                </form>
            </div>

        </div>

        </div>
    </form>

    {{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--}}
    <script>
        // $("#yearInputBox").hide();
        $(document).ready(function () {
            $("#yearInputBox").hide();
            $("#btn_no").click(function () {
                $("#yearInputBox").show();

            });
        });
    </script>

@endsection

